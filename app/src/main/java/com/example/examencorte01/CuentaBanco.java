package com.example.examencorte01;

public class CuentaBanco {
    private int numCuenta;
    private String nombre;
    private String banco;
    private float saldo;

    // Getters y Setters
    public int getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    // Constructor
    public CuentaBanco(){
        this.numCuenta=0;
        this.nombre="";
        this.banco="";
        this.saldo=0.0f;
    }

    public CuentaBanco(int numCuenta, String nombre, String banco, float saldo){
        this.numCuenta=numCuenta;
        this.nombre=nombre;
        this.banco=banco;
        this.saldo=saldo;
    }

    // Métodos
    public void depositar(float cantidad){
        this.saldo=this.saldo+cantidad;
    }

    public boolean retirar(float cantidad) {
        if (cantidad <= this.saldo) {
            this.saldo = this.saldo - cantidad;
            return true;
        } else {
            return false;
        }
    }



}
